﻿namespace lab_2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            label1 = new Label();
            textBoxRich = new TextBox();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            textBoxA = new TextBox();
            textBoxB = new TextBox();
            textBoxH = new TextBox();
            SuspendLayout();
            // 
            // button1
            // 
            button1.BackColor = SystemColors.ControlDark;
            button1.Location = new Point(22, 294);
            button1.Name = "button1";
            button1.Size = new Size(117, 61);
            button1.TabIndex = 0;
            button1.Text = "button1";
            button1.UseVisualStyleBackColor = false;
            button1.Click += button1_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 14F);
            label1.Location = new Point(22, 21);
            label1.Name = "label1";
            label1.Size = new Size(152, 25);
            label1.TabIndex = 1;
            label1.Text = "Enter range [a,b]";
            // 
            // textBoxRich
            // 
            textBoxRich.BackColor = SystemColors.MenuBar;
            textBoxRich.Location = new Point(293, 21);
            textBoxRich.Multiline = true;
            textBoxRich.Name = "textBoxRich";
            textBoxRich.ReadOnly = true;
            textBoxRich.Size = new Size(293, 261);
            textBoxRich.TabIndex = 2;
            textBoxRich.TextChanged += textBox1_TextChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 14F);
            label2.Location = new Point(22, 75);
            label2.Name = "label2";
            label2.Size = new Size(22, 25);
            label2.TabIndex = 3;
            label2.Text = "a";
            label2.Click += label2_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 14F);
            label3.Location = new Point(152, 75);
            label3.Name = "label3";
            label3.Size = new Size(23, 25);
            label3.TabIndex = 4;
            label3.Text = "b";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 14F);
            label4.Location = new Point(22, 221);
            label4.Name = "label4";
            label4.Size = new Size(23, 25);
            label4.TabIndex = 5;
            label4.Text = "h";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 14F);
            label5.Location = new Point(22, 180);
            label5.Name = "label5";
            label5.Size = new Size(72, 25);
            label5.TabIndex = 6;
            label5.Text = "Enter h";
            label5.Click += label5_Click;
            // 
            // textBoxA
            // 
            textBoxA.Location = new Point(22, 116);
            textBoxA.Name = "textBoxA";
            textBoxA.Size = new Size(51, 23);
            textBoxA.TabIndex = 7;
            // 
            // textBoxB
            // 
            textBoxB.Location = new Point(152, 116);
            textBoxB.Name = "textBoxB";
            textBoxB.Size = new Size(51, 23);
            textBoxB.TabIndex = 8;
            textBoxB.TextChanged += textBox3_TextChanged;
            // 
            // textBoxH
            // 
            textBoxH.Location = new Point(23, 253);
            textBoxH.Name = "textBoxH";
            textBoxH.Size = new Size(51, 23);
            textBoxH.TabIndex = 9;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaption;
            ClientSize = new Size(800, 450);
            Controls.Add(textBoxH);
            Controls.Add(textBoxB);
            Controls.Add(textBoxA);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(textBoxRich);
            Controls.Add(label1);
            Controls.Add(button1);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private Label label1;
        private TextBox textBoxRich;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private TextBox textBoxA;
        private TextBox textBoxB;
        private TextBox textBoxH;
    }
}
