﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, h;
            bool ok;
            ok = double.TryParse(A.Text, out a);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse( B.Text, out b);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ok = double.TryParse( H.Text, out h);
            if (!ok)
            {
                MessageBox.Show("Помилка введення значення h!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Result.Clear();
            for (double x = a; x <= b; x += h)
            {
                double y = Func(x);
                Result.AppendText($" x = {x:F3}\t\ty = {y:F3}\n");
            }
        }
        private double Func(double x)
        {
            return (Math.Pow(x, 2) + 2 * x) / (Math.Cos(5 * x) + 2);
        }

        private void A_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void B_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}