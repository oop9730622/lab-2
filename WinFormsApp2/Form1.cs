﻿namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        private Color circleColor;

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel.Visible = true;

            string input = textBox1.Text;

            if (!int.TryParse(input, out _))
            {
                MessageBox.Show("Будь ласка, введіть ціле число.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            double average = CalculateAverage(input);
           textBox2.Text = average.ToString();
            if (average > 5)
            {
                circleColor = Color.Red;
            }
            else
            {
                circleColor = Color.Green;
            }

        panel.Invalidate();
        }

        private double CalculateAverage(string number)
        {
            int sum = 0;
            foreach (char digit in number)
            {
                sum += (digit - '0');
            }
            return (double)sum / number.Length;
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush b = new SolidBrush(circleColor);

            g.FillEllipse(b, 60, 30, 85, 85);
        }
    }
}
