﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfApp2
{
    public partial class MainWindow : Window
    {
        private Brush circleBrush;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string input = textBox1.Text;

            if (!int.TryParse(input, out _))
            {
                MessageBox.Show("Будь ласка, введіть ціле число.", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            double average = CalculateAverage(input);
            textBox2.Text = average.ToString();
            if (average > 5)
            {
                circleBrush = Brushes.Red;
            }
            else
            {
                circleBrush = Brushes.Green;
            }

            DrawCircle();
        }

        private double CalculateAverage(string number)
        {
            int sum = 0;
            foreach (char digit in number)
            {
                sum += (digit - '0');
            }
            return (double)sum / number.Length;
        }

        private void DrawCircle()
        {
            canvas.Children.Clear();
            Ellipse circle = new Ellipse
            {
                Width = 85,
                Height = 85,
                Fill = circleBrush
            };
            Canvas.SetLeft(circle, 60);
            Canvas.SetTop(circle, 30);
            canvas.Children.Add(circle);
        }
    }
}
